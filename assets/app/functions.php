<?php
function getList()
{
    $pdo = new PDO('sqlite:assets/db/mydb.sqlite');
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $st = $pdo->query('SELECT * FROM tasks');
    $results = $st->fetchAll();
    $pdo = null;
    return $results;
}

function dopListForDel($id)
{
    $list = [];
    $results = getList();
    $list = dopListForDelSearch($id, $results);
    return $list;
}

function dopListForDelSearch($id, $results)
{
    static $list = [];
    for ($i = 0; $i < count($results); $i++) {
        if ($results[$i]['parent_id'] === $id) {
            $list[] = $results[$i]['id'];
            dopListForDelSearch($results[$i]['id'], $results);
        }
    }
    return $list;
}

function prepareList()
{
    $results = getList();
    $arrTask = array();

    for ($i = 0; $i < count($results); $i++) {
        $row = $results[$i];
        if (empty($arrTask[$row['parent_id']])) {
            $arrTask[$row['parent_id']] = array();
        }
        $arrTask[$row['parent_id']][] = $row;
    }
    return $arrTask;
}

function addToDB($parId, $inpText)
{
    $pdo = new PDO('sqlite:assets/db/mydb.sqlite');
    $sql = 'INSERT INTO tasks (parent_id, text) VALUES (:parId, :inpText)';
    $result = $pdo->prepare($sql);
    $result->bindParam(':parId', $parId, PDO::PARAM_INT);
    $result->bindParam(':inpText', $inpText, PDO::PARAM_STR);
    $result->execute();
    $pdo = null;
}

function delFromDB($id)
{
    $list = dopListForDel($id);
    $pdo = new PDO('sqlite:assets/db/mydb.sqlite');
    $sqlF = 'DELETE FROM tasks WHERE id = :iD';
    $resultF = $pdo->prepare($sqlF);
    $resultF->bindParam(':iD', $id, PDO::PARAM_INT);
    $resultF->execute();
    foreach ($list as $item => $value) {
        $sqlF = 'DELETE FROM tasks WHERE id = :iD';
        $resultF = $pdo->prepare($sqlF);
        $resultF->bindParam(':iD', $value, PDO::PARAM_INT);
        $resultF->execute();
    }
    $pdo = null;
}

function view_lists($arr, $parent_id = 0)
{
    static $html = '';
    if (empty($arr[$parent_id])) {
        return;
    }
    $html .= '<ul>';
    for ($i = 0; $i < count($arr[$parent_id]); $i++) {
        $html .= '<li><span id="' . $arr[$parent_id][$i]['id'] . '" contenteditable onblur="updTask(' . $arr[$parent_id][$i]['id'] . ')">' . $arr[$parent_id][$i]['text'] . '</span><input type="button" value="Add subtask" onclick="newTask(' . $arr[$parent_id][$i]['id'] . ')"><input type="button" value="Delete" onclick="delTask(' . $arr[$parent_id][$i]['id'] . ')">';
        view_lists($arr, $arr[$parent_id][$i]['id']);
        $html .= '</li>';
    }
    $html .= '</ul>';
    return $html;
}

function updateDB($parId, $inpText)
{
    $pdo = new PDO('sqlite:assets/db/mydb.sqlite');
    $sql = 'UPDATE tasks SET text = :inpText WHERE id = :parId';
    $result = $pdo->prepare($sql);
    $result->bindParam(':parId', $parId, PDO::PARAM_INT);
    $result->bindParam(':inpText', $inpText, PDO::PARAM_STR);
    $result->execute();
    $pdo = null;
}