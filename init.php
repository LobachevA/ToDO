<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/assets/app/functions.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    require_once $_SERVER['DOCUMENT_ROOT'].'/assets/view/view.php';
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $routes = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
    if (($routes[0] == 'tasks') && (is_numeric($routes[1]))) {
        $text = $_POST['text'];
        $parentId = $_POST['parent_id'];
        if (isset($_POST['type']) && ($_POST['type'] == 'PUT')) {
            updateDB($parentId, $text);
        } else {
            addToDB($parentId, $text);
        }
        $arrTask = prepareList();
        echo view_lists($arrTask);
    }
}
if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $routes = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
    if (($routes[0] == 'tasks') && (is_numeric($routes[1]))) {
        delFromDB($routes[1]);
        $arrTask = prepareList();
        echo view_lists($arrTask);
    };
}
