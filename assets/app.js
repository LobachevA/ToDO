function newTask(id) {
    var text = document.getElementById("inputText").value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', 'tasks/' + id, true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("text=" + encodeURIComponent(text) + "&parent_id=" + encodeURIComponent(id));
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                document.getElementById("todolist").innerHTML = xmlhttp.responseText;
                document.getElementById("inputText").value = '';
            }
        }
    };
}
function delTask(id) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('DELETE', 'tasks/' + id, true);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                document.getElementById("todolist").innerHTML = xmlhttp.responseText;
            }
        }
    };
}
function updTask(id) {
    var text = document.getElementById("" + id + "").innerHTML;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', 'tasks/' + id, true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("text=" + encodeURIComponent(text) + "&parent_id=" + encodeURIComponent(id) + "&type=PUT");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                document.getElementById("todolist").innerHTML = xmlhttp.responseText;
            }
        }
    };
}