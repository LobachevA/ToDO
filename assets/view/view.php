<?php
$arrTask = prepareList();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link href="/assets/main.css" rel="stylesheet">
</head>
<body>
<div>
    <form class="form" id="taskForm">
        <div class="form-group">
            <label>Type new task/subtask</label>
            <input type="text" class="form-control" name="inputText" id="inputText"/>
            <input type="button" id="addRequest" value="Add task" onclick="newTask(0)">
        </div>
    </form>
</div>
<div id="todolist">
    <?php echo view_lists($arrTask); ?>
</div>
<script src="/assets/app.js"></script>
</body>
</html>